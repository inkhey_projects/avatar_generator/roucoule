# Roucoule


An avatar generator optimised for generating random avatars based on multiple image layers, able to return same image from a string "slug".
Heavily inspired by [David Revoy](http://www.peppercarrot.com)'s [cat avatar generator](https://framagit.org/Deevad/cat-avatar-generator/) and [MonsterID by Andreas Gohr's](https://www.splitbrain.org/blog/2007-01/20_monsterid_as_gravatar_fallback).

This generator relies on the [`Image`](image) crate to do image processing.

Roucoule is a Rust Re-implementation of [Pioupiou](https://framagit.org/inkhey/pioupiou) Avatar Generator python Library. This version
rely on image library.

![](roucoule.png)

WIP. This is a project to learn rust by practice, so it's not
state of the art for now.

## Command-line usage:


```bash
cargo build
./target/debug/roucoule roucoule roucoule.png
```


cat:

```bash
cargo build
./target/debug/roucoule roucoule roucoule.png -t "sample/cat_revoy" -l "body,fur,eyes,mouth,accessorie"
```

monster:

```bash
cargo build
./target/debug/roucoule roucoule monster.png -t "sample/monster_id" -l "legs,hair,arms,body,eyes,mouth" -w 120 -i 120
```

## Lib Usage

```rust
use roucoule::AvatarGenerator;
use std::path::PathBuf;

let av = AvatarGenerator{
        output_path: PathBuf::from("test.png"),
        theme_path: PathBuf::from("sample/bird_revoy/"),
        layers: "tail,hoop,body,wing,eyes,bec,accessorie".to_string(),
        width: 256,
        height: 256
};
av.generate_avatar("test");
```
