use clap::Parser;
use roucoule::AvatarGenerator;
use std::path::PathBuf;

/// Avatar Generator
/// for generating random avatars based on multiple image layers, able to return same image from a string "slug".
#[derive(Parser)]
struct Cli {
    /// The slug of the generated avatar
    slug: String,
    /// The output path where the avatar is generated
    output_path: PathBuf,
    /// Theme path
    #[arg(short, long, default_value = "sample/bird_revoy/")]
    theme_path: PathBuf,

    /// List of layers of the image generation from the lowest to the upper layer
    /// all layers should be separated by [LAYER_SPLIT_CHAR]. Please make sure for
    /// all layers that at least one image is matching the name *{layer}_\[0-9\]\*.png* in the
    /// *output_path* directory.
    #[arg(short, long, default_value = "tail,hoop,body,wing,eyes,bec,accessorie")]
    layers: String,

    /// Width of generated image, all original image should have this width
    #[arg(short, long, default_value = "256")]
    width: u32,

    /// Height of generated image, all original image should have this width
    #[arg(short = 'i', long, default_value = "256")]
    height: u32,
}

/// Run the cli
fn main() {
    env_logger::init();
    let args = Cli::parse();
    log::info!("Generating Avatar for {}.", args.slug);
    let av = AvatarGenerator {
        output_path: args.output_path,
        theme_path: args.theme_path,
        layers: args.layers,
        width: args.width,
        height: args.height,
    };
    av.generate_avatar(args.slug.as_str());
}
